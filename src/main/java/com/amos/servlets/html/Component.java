package com.amos.servlets.html;

import java.util.ArrayList;

public abstract class Component {


    protected StringBuilder stringBuilder   = new StringBuilder();
    protected ArrayList<Attribut> attributes = new ArrayList<>();
    protected static ArrayList<Style> styles = new ArrayList<>();
    protected static final String STYLE     = "style=";
    protected static final String ID        = "id=";
    protected static final String NAME      = "name=";
    protected static final String CLASS     = "class=";
    protected String css;
    protected String id;
    protected String name;
    protected String cls;
    protected static final Br BR= new Br();


    public String getCss() {
        return css;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id="\""+id+"\"";

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name="\""+name+"\"";

    }


    protected abstract String  opedTag();
    protected abstract String  closeTag();
    public abstract String toString();
    public Component addCss(Style css){
       // styles.add(css);
        this.css="\""+css.toString()+"\"";
        return this;
    }

    public Component setClass(String cls) {
        this.cls="\""+cls+"\"";
        return this;
    }

    public static String addLink(Link link){
        return link.toString();
    }


    public static Component nextLine(){
        return BR;
    }
}

class Attribut{

   private String name;
   private String value;

    public Attribut(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return " "+name+"=\""+value+"\" ";
    }
}

class Br extends Component{


    @Override
    protected String opedTag() {
        return null;
    }

    @Override
    protected String closeTag() {
        return null;
    }

    @Override
    public String toString() {
        return "<br>";
    }

}