package com.amos.servlets.html;

public class Button extends Component {

    private String value;

    public Button(String value){
        this.value="\""+value+"\"";
    }

    public void setFunction(String fun){
        attributes.add(new Attribut("onclick",fun));
    }

    @Override
    protected String opedTag() {
        return "<input type=\"button\" value="+value;
    }

    @Override
    protected String closeTag() {
        return "/>";
    }

    @Override
    public String toString() {
        stringBuilder.append(opedTag());
        if(id!=null){
            stringBuilder.append(" "+ID+id);
        }
        if(css!=null){
            stringBuilder.append(" "+STYLE+css);
        }
        if(name!=null){
            stringBuilder.append(" "+NAME+name);
        }if(cls!=null){
            stringBuilder.append(" "+CLASS+cls);
        }
        return stringBuilder.append(closeTag()).toString();
    }
}
