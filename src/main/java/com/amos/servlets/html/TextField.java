package com.amos.servlets.html;

public class TextField extends Component {

    private String size;
    public TextField(int size){
       if(size>0){
           this.size="\""+size+"\"";
       }
    }
    public TextField(){
        this(0);
    }

    @Override
    protected String opedTag() {
        return "<input type=\"text\" ";
    }

    @Override
    protected String closeTag() {
        return "/>";
    }

    @Override
    public String toString() {
        stringBuilder.append(opedTag());

        if(id!=null){
            stringBuilder.append(" "+ID+id);
        }if(css!=null){
            stringBuilder.append(" "+STYLE+css);
        }if(cls!=null){
            stringBuilder.append(" "+CLASS+cls);
        }
        if(size!=null)
         stringBuilder.append(" size="+size);
        stringBuilder.append(closeTag());
        return stringBuilder.toString();
    }
}
