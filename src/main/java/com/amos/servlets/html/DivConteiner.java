package com.amos.servlets.html;

import java.util.ArrayList;
import java.util.List;

public class DivConteiner extends Component implements Conteiner {


    protected List<Component> components= new ArrayList<>();

    @Override
    protected String opedTag() {
        return "<div";
    }

    @Override
    protected String closeTag() {
        return "</div>";
    }

    @Override
    public String toString() {
        stringBuilder.append(opedTag());
        if(id!=null){
            stringBuilder.append(" "+ID+id);
        }
        if(css!=null){
            stringBuilder.append(" "+STYLE+css);

        }if(cls!=null){
            stringBuilder.append(" "+CLASS+cls);
        }
        stringBuilder.append(">");
        components.forEach((Component e)->stringBuilder.append(e.toString()));
        return stringBuilder.append(closeTag()).toString();
    }

    @Override
    public void add(Component component) {
        components.add(component);
    }
}
