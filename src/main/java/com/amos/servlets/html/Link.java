package com.amos.servlets.html;

import java.util.ArrayList;
import java.util.List;

public class Link extends Component implements Conteiner,Clicked{

    private String resource;
    private String label;
    private String onclick;
    private List<Component> innerComponents= new ArrayList<>();
    boolean newTab;
    public static final String OPEN_TAG="<a";

    public static final String CLOSE_TAG="</a>";
    public Link( String label,String resource,boolean http) {
        if(label==null)
            throw new IllegalArgumentException();
            if(resource!=null) {
                if (http)
                    if (!resource.startsWith("http://") || !resource.startsWith("https://")) {
                        resource = "http://" + resource;
                    }
                this.resource="\""+resource+"\"";
            }


            this.label = label;
    }


    public Link setNewTab(boolean newTab){
        this.newTab=newTab;
        return this;
    }
    public Link( String label,String resource) {
        this(label,resource,false);
    }

    @Override
    protected String opedTag() {
        return null;
    }

    @Override
    protected String closeTag() {
        return null;
    }

    @Override
    public String toString() {
        stringBuilder.append(OPEN_TAG);
        if(newTab){
            stringBuilder.append(" target=\"_blank\"");
        }
        stringBuilder.append(" href="+resource);
        if(cls!=null){
            stringBuilder.append(" class="+cls);
        }

        if(id!=null){
            stringBuilder.append(" id="+id);
        }
        if(css!=null){
            stringBuilder.append(" "+STYLE+css);
        }
        if(onclick!=null)
            stringBuilder.append(" "+ONCLICK+onclick);

        stringBuilder.append(">"+label);
        stringBuilder.append(CLOSE_TAG);
        return stringBuilder.toString();
    }

    @Override
    public void add(Component component) {
         innerComponents.add(component);
    }

    @Override
    public void onclick(String event) {
        onclick="\""+event+"\"";
    }
}
