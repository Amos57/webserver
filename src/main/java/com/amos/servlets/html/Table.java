package com.amos.servlets.html;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Consumer;

public class Table extends Component{

    public class Row implements Iterable{
        private Object[] obj;
        public Row(Object...object){
            obj=object;
        }


        @Override
        public Iterator iterator() {
            return new Iter();
        }
        class Iter implements Iterator{

              int i=0;
            @Override
            public boolean hasNext() {
                return i!=obj.length;
            }

            @Override
            public Object next() {
                if(i>=obj.length)
                    return obj[obj.length-1];
                return obj[i++];
            }
        }
    }

    private static final String OPEN_TABLE = "<table";
    private static final String CLOSE_TABLE= "</table>";
    private static final String BORDER     = "border=";


    private int border;
    private String caption;
    private ArrayList<Row> arrayList;



    private String idTd;
    private String classTd;
    private String idTr;
    private String classTr;
    private Consumer<Row> task;
    public Table(String...title){
       if(title.length!=0){
           caption="";
           for(String ttle : title){
               caption+="<th>"+ttle+"</th>";
           }

       }
        stringBuilder.append(OPEN_TABLE);
        stringBuilder.append(" ");
        arrayList= new ArrayList<>();
    }

    @Override
    protected String opedTag() {
        return null;
    }

    @Override
    protected String closeTag() {
        return null;
    }

    public String toString() {

        if(id!=null){
            stringBuilder.append(" "+ID+id);
        }if(cls!=null){
            stringBuilder.append(" "+CLASS+cls);
        }
        if(border!=0){
            stringBuilder.append(" "+BORDER+"\""+border+"\"");
        }

        stringBuilder.append(">");
        if(caption!=null){
            stringBuilder.append(caption);
        }


        arrayList.forEach((Row r)-> {

            stringBuilder.append("<tr>");
            for(Object o : r){
                stringBuilder.append("<td>"+o+"</td>");
            }
            stringBuilder.append("</tr>");
        });


        stringBuilder.append(CLOSE_TABLE);
        return stringBuilder.toString();
    }

    public void setBorder(int border) {
        this.border = border;
    }

    public void addRow(Row row){
        arrayList.add(row);
    }

    public void addRows(Row...row){
        for (int i = 0; i <row.length ; i++) {
            arrayList.add(row[i]);
        }

    }

    public void addValues(Object...r){
        Row row= new Row(r);
            arrayList.add(row);
    }

    public void addObject(Object row){
        if(row==null)
            throw new NullPointerException();
        Class cls=row.getClass();

        Field[] fields=cls.getDeclaredFields();
        Object[] ob= new Object[fields.length];
        int i=0;
        for(Field field : fields){
            field.setAccessible(true);
            try {
                ob[i]=field.get(row);
            } catch (IllegalAccessException e) { }

            field.setAccessible(false);
            i++;
        }
        arrayList.add(new Row(ob));
    }
    public void setIdTd(String idTd) {
        this.idTd = idTd;
    }

    public void setClassTd(String classTd) {
        this.classTd = classTd;
    }

    public void setIdTr(String idTr) {
        this.idTr = idTr;
    }

    public void setClassTr(String classTr) {
        this.classTr = classTr;
    }
    public void setCaption(String caption) {
           this.caption = "<caption>"+caption+"</caption>";
    }
}
