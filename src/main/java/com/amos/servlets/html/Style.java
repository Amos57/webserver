package com.amos.servlets.html;

import java.util.HashMap;
import java.util.Map;

public class Style {

    private Map<String,Object> css= new HashMap<>();
    private String tagName;
    private static final String FORMAT_STYLE = "%s: %s;";

    public void put(String s,Object val){
        css.put(s,val);
    }

    public Style(String name){
        this.tagName=name;
    }
    public Style(){
        this(null);
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(tagName);
        sb.append("{");
        for(Map.Entry entry : css.entrySet()){
             sb.append(String.format(FORMAT_STYLE,entry.getKey(),String.valueOf(entry.getValue()))) ;
        }
        sb.append("}");
        return sb.toString();
    }
}
