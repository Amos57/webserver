package com.amos.servlets.html;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Panel extends DivConteiner {

    private ArrayList<String> resourcess= new ArrayList<>();


    private ArrayList<String> includingJavascript= new ArrayList<>();
    private ArrayList<String> includingCss= new ArrayList<>();
    private String title;



    public Panel(String title){
        stringBuilder.append("<!DOCTYPE html>");
        stringBuilder.append(opedTag());
        stringBuilder.append("<head>");
        stringBuilder.append("<head>");
        if(title!=null) {
            this.title = "<title>"+ title+"</title>";
        }
    }

    public Panel(){
        this("untitle");
    }

    public void setTitle(String title) {
        this.title = "<title>"+ title+"</title>";
    }

    public  void insertBR(){
        components.add(BR);
    }
    public void iframe(Panel panel){}


    public void addStyleLink(String url){
       String tag=" <link rel = \"stylesheet\" type = \"text/css\" href = \""+url+"\"/>";
        resourcess.add(tag);
    }

    public void addJavascriptLink(String url){
        String tag=" <link rel = \"script\" type = \"text/javascript\" href = \""+url+"\"/>";
        resourcess.add(tag);
    }
    public void addJQuery(String url){
        String tag=" <script type = \"text/javascript\" src = \""+url+"\"/></script>";
        resourcess.add(tag);
    }

    public void includeJavascript(File file) throws IOException {
        StringBuilder sb= new StringBuilder();
        sb.append("<script type=\"text/javascript\">");
        byte[] js= Files.readAllBytes(Paths.get(file.getPath()));
        sb.append(new String(js));
        sb.append("</script>");
        includingJavascript.add(sb.toString());
    }

    public void includeCss(File file) throws IOException {
        StringBuilder sb= new StringBuilder();
        sb.append("<style type=\"text/css\">");
        byte[] css= Files.readAllBytes(Paths.get(file.getPath()));
        sb.append(new String(css));
        sb.append("</style>");
        includingCss.add(sb.toString());
    }

    @Override
    protected String opedTag() {
        return "<html>";
    }

    @Override
    protected String closeTag() {
        return "</html>";
    }

    @Override
    public String toString() {
        stringBuilder.append(title);
        resourcess.forEach((e)->stringBuilder.append(e));
        stringBuilder.append("</head>");
        includingCss.forEach((e)->stringBuilder.append(e));
        includingJavascript.forEach((e)->stringBuilder.append(e));
        stringBuilder.append("<body>");
        components.forEach((e)-> stringBuilder.append(e.toString()));
        stringBuilder.append("</body>");
        return stringBuilder.append(closeTag()).toString();
    }
}
