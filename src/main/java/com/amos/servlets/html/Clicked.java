package com.amos.servlets.html;

public interface Clicked {

    static final String ONCLICK="onclick=";

    void onclick(String event);
}
