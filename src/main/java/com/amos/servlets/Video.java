package com.amos.servlets;


import com.amos.annotation.Servlet;
import com.amos.http.AbstractServlet;
import com.amos.http.Request;
import com.amos.http.Response;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Exchanger;

@Servlet(url = "video")
public class Video extends AbstractServlet {


    private Webcam webcam = null;
    private BufferedImage image = null;
    private Exchanger<String> exchanger = new Exchanger<String>();

    @Override
    public void doGet(Request request, Response resp) throws IOException {
        Dimension[] dimensions={WebcamResolution.VGA.getSize()};
        webcam = Webcam.getDefault();
        webcam.setCustomViewSizes(dimensions);
         webcam.setViewSize(WebcamResolution.VGA.getSize());
        webcam.open();
        read(resp);
        webcam.close();
    }

    private void read(Response response) {


           //if (!webcam.isOpen()) {
           //    return;
           //}
           //if ((image = webcam.getImage()) == null) {
           //    return;
           //}

            try {

                image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ImageIO.write(image, "jpg", outputStream);
                data = outputStream.toByteArray();

                response.getOutputStream().write(data);


            } catch (Exception e) {
                e.printStackTrace();
            }

    }

    @Override
    public String toString() {
        return null;
    }
}
