package com.amos.templetes;

import com.amos.annotation.Init;
import com.amos.annotation.Template;
import com.amos.annotation.TemplateInjection;

import javax.swing.*;
import java.util.ArrayList;

 class Test{
  int id;



     JFrame ss=new JFrame();

     Test(){
  int i=ss.EXIT_ON_CLOSE;
     }
}
@Template
public class TestBean {


   @TemplateInjection
   private InnerBean innerBean;


   private String title="MyTitle";
   private  String message="Hello world";

   private ArrayList<Test> list=new ArrayList<>();

   @Init
   private void init() throws Exception {
      message = innerBean.ss;
     list.clear();
      for (int i=0;i<10;i++){
          Test t= new Test();
          t.id=i;
          list.add(t);
      }

   }
}
