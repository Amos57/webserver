package com.amos.templetes;

import com.amos.annotation.Init;
import com.amos.annotation.Template;
import com.amos.annotation.TemplateInjection;
import com.amos.http.LoadPage;
import com.amos.http.Util;
import com.amos.reflect.ReflectUtils;

import java.io.*;
import java.lang.reflect.Field;

import java.lang.reflect.InvocationTargetException;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TempleitManager implements LoadPage {

    private static final String REGEX="\\{(.*?)\\}";
    private static Map<String,Object> templetes= new HashMap<>();

    private  boolean session=false;
    public TempleitManager() {

        Set<Class<?>> templetes = ReflectUtils.getClassesByAnnotation(Template.class);

        for (Class cls : templetes) {
            Template target = (Template) cls.getAnnotation(Template.class);

            String name = target.name();
            if ("".equals(name))
                name = cls.getSimpleName();
            Object o =ReflectUtils.newInstance(cls);
            this.templetes.put(name, o);
        }
    }


    private static String getValue(Object val,String name) throws NoSuchFieldException{
            return String.valueOf(ReflectUtils.getFieldValueByName(val,name));
    }


    private int putValueInTamplate(InputStream fis,ArrayList<Byte> mainPage,int pos) throws IOException, NoSuchTemplateException, InvocationTargetException, NoSuchFieldException {
        ArrayList<Byte> bytes= new ArrayList<>();
      //  ByteArrayOutputStream baos= new ByteArrayOutputStream();
        byte[] res=null;
        final byte[] nextLine={'('};
        boolean array=false;
        final byte[] end="}".getBytes();
        int b;
        while((b=(byte)fis.read())!=end[0] && b!=-1){
            bytes.add((byte) b);
            if(b==nextLine[0] ){
                array = true;
                break;
            }
        }
        if(b==-1) {
            mainPage.addAll(bytes);
            return mainPage.size();
        }

        if(!array){
            String[] block=new String(Util.byteListToPrimytivArray(bytes)).split("\\.");
            String className=block[0];
            String varName=block[1];
            mainPage.remove(pos-1);
            Object o= templetes.get(className);
            if(o==null)
                throw new NoSuchTemplateException("Template not found - "+className);

            List<Field> fields=ReflectUtils.getFiledByAnnotation(o, TemplateInjection.class);
            if(session) {
                for(Field f :fields){
                    String name=f.getType().getAnnotation(Template.class).name().equals("") ? f.getType().getSimpleName() : f.getType().getAnnotation(Template.class).name();
                    ReflectUtils.setValueFiled(o,f,templetes.get(name));

                }


                ReflectUtils.collMethodByAnnotation(o, Init.class);
                session=false;
            }

            String val=getValue(o,varName);
            if(val==null)
                res= "null".getBytes();
            else
                res=val.getBytes();
            Util.insertArrayToList(res,mainPage);
            return mainPage.size();


        }else if (bytes.get(0) == 'f' && bytes.get(1) == 'o' && bytes.get(2) == 'r') {
                   Stack<Byte> stack= new Stack<>();
                   stack.push((byte)'{');
                    while((b=(byte)fis.read())!=-1  ){
                        if(b=='{'){
                            stack.push((byte)'{');
                        }else if(b=='}'){
                            stack.pop();
                        }
                        if(stack.size()==0){
                            break;
                        }
                        bytes.add((byte) b);
                    }
                    if(b==-1) {
                        mainPage.addAll(bytes);
                        return mainPage.size();
                    }


                     mainPage.remove(pos-1);
                     String block=new String(Util.byteListToPrimytivArray(bytes));
                     block=block.replaceAll("\r","");
                     String dataBlock=block.substring(block.indexOf("(")+1,block.indexOf(")"));

                     dataBlock=Util.removeSpace(dataBlock);

                     int st,en=dataBlock.lastIndexOf(":");
                     String obj=dataBlock.substring(0,st=dataBlock.indexOf("."));
                     String filed=dataBlock.substring(st+1,en);



                     Object o= templetes.get(obj);

                     if(o==null)
                         throw new NoSuchTemplateException("Template not found - "+obj);
                     if(session){

                       ReflectUtils.collMethodByAnnotation(o,Init.class);
                         session= false;
                    }
                    Object fieldValue=ReflectUtils.getFieldValueByName(o,filed);
                    if(fieldValue.getClass().isArray()){


                    }else if(fieldValue instanceof Collection){
                        StringBuilder buffer = new StringBuilder();


                        String page=block.substring(block.indexOf(")")+1);
                        List<Block> blocks= getBlocks(page);

                        for(Object values : (Collection)fieldValue){
                            String p=page;
                            for(Block bloc : blocks){

                                if(!bloc.isContainsPoint){
                                    p=p.replaceFirst(bloc.block,values.toString());


                                }else{

                                        p=p.replaceAll(bloc.block,String.valueOf(ReflectUtils.getValueFromField(values,bloc.var)));


                            }
                                }

                            buffer.append(p);

                        }
                        Util.insertArrayToList(buffer.toString().getBytes(),mainPage);

                    }else{
                        throw new InvalidObjectException("filed is not array type or java.util.Collection");
                    }

            }else if(bytes.get(0) == 'i' && bytes.get(1) == 'f'){
            Stack<Byte> stack= new Stack<>();
            stack.push((byte)'{');
            while((b=(byte)fis.read())!=-1  ){
                if(b=='{'){
                    stack.push((byte)'{');
                }else if(b=='}'){
                    stack.pop();
                }
                if(stack.size()==0){
                    break;
                }
                bytes.add((byte) b);
            }
            if(b==-1) {
                mainPage.addAll(bytes);
                return mainPage.size();
            }


            mainPage.remove(pos-1);
            String block=new String(Util.byteListToPrimytivArray(bytes));
            block=block.replaceAll("\r","");
            System.out.println(block);
            }else{
            mainPage.add((byte)'{');
            mainPage.addAll(bytes);
        }
        return mainPage.size();
    }
    public  byte[] process(File page) throws NoSuchTemplateException, NoSuchElementException, NoSuchFieldException, FileNotFoundException, InvocationTargetException {
        session=true;
        ArrayList<Byte> bytes= new ArrayList<>();
        byte[] in="#{".getBytes();
        byte[] end="}".getBytes();

        try(FileInputStream fis= new FileInputStream(page)){
                 byte b;
                 int pos=0;
               m:  while((b=(byte)fis.read())!=-1){
                     if(b==in[1] && bytes.get(pos-1)==in[0]) {
                        pos= putValueInTamplate(fis,bytes,pos)-1;

                  }else
                     bytes.add(b);
                  pos++;

                 }
        }catch (IOException e){
            e.printStackTrace();
           throw new FileNotFoundException("File:" +page.getName()+" not found");
        }

        session=false;
        return Util.byteListToPrimytivArray(bytes);
    }
    private List<Block> getBlocks(String data){
        Pattern pattern=Pattern.compile(REGEX);
        Matcher matcher=pattern.matcher(data);
        List<Block> blocks= new ArrayList<>();
        while (matcher.find()){
            blocks.add(new Block(data.substring(matcher.start(),matcher.end())));
        }
        return blocks;
    }
    private class Block{
        boolean isContainsPoint;
        String block;
        String var;
        Block(String b){
            block=b;
            int p;
            isContainsPoint = (p=b.indexOf("."))!=-1;

            if(p!=-1)
              var=block.substring(p+1,block.indexOf("}"));

            block="\\"+block;
        }

       @Override
       public String toString() {
           return "{block:"+block+",var:"+var+"}";
       }
   }
}
