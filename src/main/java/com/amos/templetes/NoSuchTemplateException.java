package com.amos.templetes;

public class NoSuchTemplateException extends Exception {

    public NoSuchTemplateException(String txt){
        super(txt);
    }
}
