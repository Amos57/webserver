package com.amos.run;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.amos.config.Startup;
import com.amos.http.*;



public class Run{
	

	private Request request;
	private Response response;
	private Startup startup;
    private static LoadPage loadPage=Startup.getLoadPage();

	public Run(Socket socket) throws  IOException {
		startup = Startup.getInstance();
		request= new HttpRequest(socket);
		response=new HttpResponse(socket);
	}

	public void run() throws IOException {
		if(request.getResourcessName()==null)
			return;
		String resource=request.getResourcessName();
		AbstractServlet abstractServlet=null;
		if("/".equals(resource) && startup.getWelcomFile()!=null) {
			resource = startup.getWelcomFile();

		}
		 if((abstractServlet=startup.getServlet(resource.substring(1)))!=null) {
           switch (request.getMethod()){
			   case "GET":
				   abstractServlet.doGet(request, response);
				   break;
			   case "POST":
				   abstractServlet.doPost(request, response);
				   break;
			   case "PUT":
				   abstractServlet.doPut(request, response);
				   break;
			   case "DELETE":
				   abstractServlet.doDelete(request, response);
				   break;
		   }
			response.addDateHeader("Server", System.currentTimeMillis());
			response.getWriter().flush();
			if (abstractServlet.getData() != null) {
				response.addIntHeader("Content-Length", abstractServlet.getData().length);
				response.getOutputStream().write(response.buildHeder().getBytes());
				response.getOutputStream().write(abstractServlet.getData());
				response.getWriter().flush();
			}else if(response.getBufferSize()!=0){
				response.addIntHeader("Content-Length", response.getBufferSize());
				response.getOutputStream().write(response.buildHeder().getBytes());
				response.getOutputStream().write(response.getBuffer());
				response.getWriter().flush();

			}
			return;
		}else{//main if
			File file = new File(Startup.RESOURCES + File.separator + resource);
			byte[] data = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			String typeDocument=request.getHeader("Sec-Fetch-Dest")!=null ? request.getHeader("Sec-Fetch-Dest"):"document";

			if(file.exists() && file.isFile()){
				if(typeDocument.equals("document")){
					try {
						data = loadPage.process(file);
					} catch (Exception e) {
						response.setStatus(Response.SC_EXPECTATION_FAILED);
						data = Util.stackTraceString(e).getBytes();
						e.printStackTrace();
					}
					response.addHeader("Content-Type", "text/html");
				}else{
					data=Files.readAllBytes(Paths.get(file.getPath()));
					if(typeDocument.equals("style")) {
						response.addHeader("Content-Type", "text/css");
					}else if(typeDocument.equals("scrypt")){
						response.addHeader("Content-Type", "text/javascript");
					}
				}
			}else{

				response.setStatus(Response.SC_NOT_FOUND);
				data = Util.fileToBytes(Startup.RESOURCES + File.separator + "404.html");
			}



			response.addIntHeader("Content-Length", data.length);
			response.addDateHeader("Server", System.currentTimeMillis());
			baos.write(response.buildHeder().getBytes());
			baos.write(data);
			response.getOutputStream().write(baos.toByteArray());
			response.getOutputStream().flush();
			request.getReader().close();
			response.getWriter().close();
		}



	}
}



