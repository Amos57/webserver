package com.amos.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


import java.util.*;



import com.amos.annotation.Servlet;

import com.amos.http.AbstractServlet;
import com.amos.http.LoadPage;
import com.amos.json.*;
import com.amos.reflect.ReflectUtils;



public class Startup {

	
	
	private static Startup startup;
	private int port;
	private Map<String,AbstractServlet> classes;
	private Map<String,Object> templetes;
	private String welcomFile;
	
	public static final String RESOURCES="res";
	public static final String NOT_FOUND_FILE="404.html";
	public static final String NOT_FOUND= Startup.RESOURCES+ File.separator+NOT_FOUND_FILE;
    private static LoadPage loadPage;

	private Startup(){
		
	    JsonObject main = null;
		try {
			main =  new JsonParser().parse(new FileInputStream("config/config.json")).getAsJsonObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    JsonPrimitive port= main.get("port").getAsJsonPrimitive();
		JsonPrimitive template= main.get("template").getAsJsonPrimitive();
	    this.port =port.asInt();
		JsonElement welcFile=main.get("welcome_file");
		if(welcFile!=null)
			welcomFile=welcFile.getAsJsonPrimitive().asString();
	    classes= new HashMap<String, AbstractServlet>();
	    if(template.asBoolean()){
	    	loadPage = (LoadPage) ReflectUtils.newInstance("com.amos.templetes.TempleitManager");
		}
	}
	
	public static Startup getInstance(){
		return startup ==null ? startup =new Startup() : startup;
	}
	
	public int getPort(){
		return port;
	}
	
	public AbstractServlet getServlet(String url){
		return classes.get(url);
	}

	public String getWelcomFile(){
		return welcomFile;
	}

	public void scannerClasses() {



		Set<Class<?>> servlets = ReflectUtils.getClassesByAnnotation(Servlet.class);


		for(Class cls: servlets) {
			try {
				AbstractServlet o = (AbstractServlet) cls.newInstance();
				o.init();
				classes.put(o.getUrl(), o);

			} catch (Exception e) {
			}
		}
		}

	public static LoadPage getLoadPage(){
		return loadPage;
	}
}
