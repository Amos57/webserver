package com.amos.reflect;

public class InvalidTextFiledException extends Exception {

    InvalidTextFiledException(String txt){
        super(txt);
    }
}
