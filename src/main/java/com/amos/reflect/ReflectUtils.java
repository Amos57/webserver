package com.amos.reflect;

import com.amos.Main;
import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ReflectUtils {

   private static Reflections reflections = new Reflections(Main.class.getPackage().getName());


    public static Object getFieldValueByName(Object tepleit,String fieldName) throws NoSuchFieldException {
        Object value=null;
       try {
           Field field = tepleit.getClass().getDeclaredField(fieldName);
           field.setAccessible(true);
            value = field.get(tepleit);
           field.setAccessible(false);
       }catch (IllegalAccessException e){

       }
        return value;
    }

    public static List<Field> getFiledByAnnotation(Object obj, Class<? extends Annotation> annotationType)  {
        Field[] field=obj.getClass().getDeclaredFields();
        List<Field> list= new ArrayList<>();
        for(int i=0;i<field.length;i++){
            Annotation annotation=field[i].getAnnotation(annotationType);
            if(annotation!=null)
                list.add(field[i]);
        }
        return list;
    }

public static void setValueFiled(Object o,Field f,Object val){
        f.setAccessible(true);
    try {
        f.set(o,val);
    } catch (IllegalAccessException e) {
        e.printStackTrace();
    }
    f.setAccessible(false);
}

    public static Method collMethodByAnnotation(Object o, Class<? extends Annotation> annotationType,Object...obj) throws InvocationTargetException  {
        Method[] m=o.getClass().getDeclaredMethods();

        for(Method method : m){
            Annotation annotation=method.getAnnotation(annotationType);
            if(annotation!=null){
               method.setAccessible(true);
                try {
                    if(obj!=null)
                        method.invoke(o,obj);
                    else
                        method.invoke(o);
              }catch (IllegalAccessException e){
                  e.getStackTrace();
                    method.setAccessible(false);
                    return method;

              } catch (InvocationTargetException e) {
                    e.printStackTrace();
                    method.setAccessible(false);
                    throw new InvocationTargetException(e);

              }finally {
                    return method;
                }

            }
        }
        return null;
    }

    public static void collMethodByName(Object o, String name,Object[] obj,Class...cls)
            throws InvocationTargetException, NoSuchMethodException {
        Method method=o.getClass().getDeclaredMethod(name,cls);

                method.setAccessible(true);
                try {
                    if(obj!=null)
                        method.invoke(o,obj);
                    else
                        method.invoke(o);
                }catch (IllegalAccessException e){}
                method.setAccessible(false);

    }

    public static void collMethodByName(Object o, String name) throws InvocationTargetException, NoSuchMethodException {
        collMethodByName(o,name,null);
    }





public static Set<Class<?>> getClassesByAnnotation(Class<? extends Annotation> cls){
        return reflections.getTypesAnnotatedWith(cls);
}


public static Object newInstance(String classpath){
    try {
        return Class.forName(classpath).newInstance();
    } catch (InstantiationException e) {
        e.printStackTrace();
    } catch (IllegalAccessException e) {
        e.printStackTrace();
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    }
    return null;
}

    public static Object newInstance(String classpath,Object[] val,Class...type){
        try {
            return Class.forName(classpath).getDeclaredConstructor(type).newInstance(val);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object newInstance(Class cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return  null;
    }


    public static Object getValueFromField(Object obj,String names) throws NoSuchFieldException {
        if(names==null){
            throw new NullPointerException("Invalid text value format "+names);
        }
        String[] val=names.split("\\.");

        if(val.length==1){
          return getFieldValueByName(obj,names);
        }

        Object res=getFieldValueByName(obj,val[1]);
        for(int i=2;i<val.length;i++){
            res=getFieldValueByName(res,val[i]);
        }
        return res;
    }
}
