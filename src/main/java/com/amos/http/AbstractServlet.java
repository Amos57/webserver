package com.amos.http;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import com.amos.annotation.Servlet;

public abstract class AbstractServlet {

	
	protected String url;

    protected byte[] data=null;

	protected AbstractServlet(){
		
		Servlet annotation=this.getClass().getAnnotation(Servlet.class);

			try {
			url=	annotation.url();
			} catch (SecurityException e) {e.printStackTrace();
			} catch (IllegalArgumentException e) {e.printStackTrace();
			}
			init();
	}
	
	public void init(){}
	public void destroy(){}
	
	public abstract String toString();
	
	public void doGet(Request request,Response resp) throws IOException{}
	public void doPost(Request request,Response resp)throws IOException{}
	public void doDelete(Request request,Response resp)throws IOException{}
	public void doPut(Request request,Response resp)throws IOException{}

	public String getUrl() {
		return url;
	}
	
	public RequestDispetcher getRequestDispetcher(Request request,Response resp){
		return RequestDispetcher.getInstance().setOutputInput(request, resp);
	}
	public byte[] getData(){
		return data;
	}
}
