package com.amos.http;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;


import com.amos.config.Startup;

public class RequestDispetcher {

	
	private static RequestDispetcher init;
	private Request request;
	private Response response;
	private Startup config;
	
	private RequestDispetcher(){
     config=Startup.getInstance();
	}

	public void forward(String url){
		String servletname=
				url==null ?  "" : url;
		if("".equals(servletname) && config.getWelcomFile()!=null){
			servletname=config.getWelcomFile();
		}

		AbstractServlet abstractServlet=config.getServlet(servletname);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if(abstractServlet!=null){

			switch (request.getMethod()) {
			case "GET":
				try {
					abstractServlet.doGet(request, response);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case "POST":
				try {
					abstractServlet.doPost(request, response);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			try {
				response.getWriter().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			
			   byte[] page=null;
			try {
				page = Util.fileToBytes(Startup.RESOURCES+File.separator+servletname);
			} catch (IOException e) {
				try {
					response.setStatus(Response.SC_NOT_FOUND);
					page = Util.fileToBytes(Startup.NOT_FOUND);
				} catch (IOException e1) {}
			}
			
			
			response.addDateHeader("Server", System.currentTimeMillis());
	        response.addHeader("Content-Type", "text/html");         
	        response.addIntHeader("Content-Length" , page.length);
	        
	           try {
	        	baos.write(response.buildHeder().getBytes());
				baos.write(page);
				response.getOutputStream().write(baos.toByteArray());
				response.getOutputStream().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
		
	
	
	public RequestDispetcher setOutputInput(Request request,Response response){
		this.request=request;
		this.response=response;
		return init;
	}
	
	public static RequestDispetcher getInstance(){
			return init== null ? init= new RequestDispetcher() : init;
	}

}
