package com.amos.http;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Util {

	
	private Util(){}
	
	
	public static byte[] fileToBytes(String path) throws IOException{

		return	Files.readAllBytes(Paths.get(path));
	}
	public static byte[] fileToBytes(File path) throws IOException{

		return	Files.readAllBytes(Paths.get(path.getPath()));
	}
	static int ITERATOR=0;
	public synchronized static byte[] byteListToPrimytivArray(List<Byte> collection){
		byte[] b= new byte[collection.size()];

		collection.forEach((e)->{b[ITERATOR]=collection.get(ITERATOR);ITERATOR++;});
		ITERATOR=0;
		return  b;
	}
public static void insertArrayToList(byte[] array,List<Byte> list){
		for(byte b : array) list.add(b);
}
//public synchronized List<Byte> listToPrimytivArray(List<Byte> collection){
//	byte[] b= new byte[collection.size()];

//	collection.forEach((e)->{b[ITERATOR]=collection.get(ITERATOR);ITERATOR++;});
//	ITERATOR=0;
//	return  b;
//}


	public static boolean inalidCharacters(byte b,byte...val){
		for(byte v: val)
			if(b==v)
				return false;
			return true;
	}


	public static  String stackTraceString(Throwable e){
		StringBuilder sb = new StringBuilder();
		sb.append("<font size=\"5\" color=\"red\">");
		sb.append(e.getClass().getTypeName()+": "+e.getMessage()+"<br/>");
		for (StackTraceElement stackTraceElement : e.getStackTrace()) {
			String msg=stackTraceElement.toString();

			sb.append("&nbsp&nbsp&nbsp at "+msg+"<br/>");
			int st=sb.lastIndexOf("(");
			int end=sb.lastIndexOf(")");

			sb.insert(st+1,"<a href=\"#\">");
			sb.insert(end+12,"</a>");
		}
		return sb.append("</font>").toString();
	}

    public static<T> String arrayToString(T[] block) {
		StringBuilder sb= new StringBuilder();
		for (T t: block) {
			sb.append(t+",");
		}
		return sb.delete(sb.length()-1,sb.length()).toString();
	}

	public static String arrayToString(int st,int offset,Object t) {
		StringBuilder sb= new StringBuilder();
		if(t instanceof byte[]) {
			byte[] r= (byte[]) t;
			for (int i = st; i < st + offset; i++) {
				sb.append(String.format("%x, ",r[i]) );
			}
		}
		return sb.delete(sb.length()-1,sb.length()).toString();
	}
	public static String removeSpace(String str){
		return str.replaceAll("\\s+","");
	}

public static Object subArray(int st,int offset,Object array){
     byte[] ar= new byte[offset];
	if(array instanceof byte[]) {
		byte[] r= (byte[]) array;
		for (int i = st,j=0; i < st + offset;j++, i++) {
           ar[j]=r[i];
		}
		return ar;
	}
	return null;
}
}
