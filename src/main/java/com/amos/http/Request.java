package com.amos.http;

import com.amos.json.JsonElement;
import com.amos.json.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Map;

public interface Request {

	String getCharacterEncoding();
	void setCharacterEncoding(String env) throws UnsupportedEncodingException;
	int getContentLength();
	String getContentType();
	String getParameter(String name);
	Enumeration<String> getParameterNames();
	public String[] getParameterValues(String name);

    public Map<String, String[]> getParameterMap();
    
    public String getProtocol();

    public String getScheme();

    public String getServerName();

    public int getServerPort();
  
    public BufferedReader getReader() throws IOException;
    
    public String getRemoteAddr();

    public String getRemoteHost();
  
    public void setAttribute(String name, Object o);

    public void removeAttribute(String name);
    int getRemotePort();
    String getLocalName();       
    public String getLocalAddr();
    String getRemoteUser();
    String getQueryString();
    String getResourcessName();
    String getMethod();
    String getHeader(String name);
    Cookie[] getCookies();
    public int getLocalPort();
    String getUserAgent();
    Session  getSession();
    JsonObject getJson();
    boolean isImage();



}
