package com.amos.http;

public class ServletException extends ClassNotFoundException {

	public ServletException(String msg){
		super(msg);
	}
	
}
