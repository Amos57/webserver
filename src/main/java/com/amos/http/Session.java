package com.amos.http;

import java.util.*;

public class Session {

    private static Session instance;

    public static Session getInstance(){
        return instance==null ? instance= new Session() : instance;
    }

    private Session(){}

   private class Node{
        private long time = System.currentTimeMillis();
        private Object value;
        Node(Object obj){
            this.value=obj;
        }

    }
    private static  long DEFAULT_TIME= 30*60*1000;


    private  Map<String,Node> nodes= new java.util.concurrent.ConcurrentHashMap<String,Node>();



    public synchronized Object getAttribute(String name){
        Node n= nodes.get(name);
        if(n!=null) {
            if(System.currentTimeMillis()-n.time>=DEFAULT_TIME){
                nodes.remove(name);
                return null;
            }
            n.time=System.currentTimeMillis();
            return n.value;
        }
        else return null;
    }

    public synchronized void setDefaultTime(long time){
        DEFAULT_TIME=time;
    }


    public synchronized void putValue(String name, Object value){
        if(value==null)
            throw new NullPointerException("object is null");
        Node node= new Node(value);
        nodes.put(name,node);
    }

    public  synchronized void setAttribute(String name, Object value){
      //  Node n= nodes.get(name);
      //  if(n!=null) {
      //      n.value = value;
        Node n= new Node(value);
          nodes.put(name, n);
      //  }
    }

    public synchronized void removeAttribute(String name){
        nodes.remove(name);
    }
    public Set<String> getAttributeNames(){
        return nodes.keySet();
    }
}
