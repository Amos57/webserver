package com.amos.http;

import com.amos.annotation.Servlet;
import com.amos.reflect.ReflectUtils;
import com.amos.templetes.NoSuchTemplateException;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;


public interface LoadPage {

    byte[] process(File file) throws FileNotFoundException, NoSuchTemplateException, NoSuchFieldException, InvocationTargetException;
   // byte[] process(String file) throws FileNotFoundException;


}
