package com.amos.http;

import com.amos.templetes.NoSuchTemplateException;

import java.io.File;
import java.io.FileNotFoundException;

public class LoadPageDefault implements LoadPage {
    @Override
    public byte[] process(File file) throws FileNotFoundException, NoSuchTemplateException, NoSuchFieldException {
        return new byte[0];
    }
}
