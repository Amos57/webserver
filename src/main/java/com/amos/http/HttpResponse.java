package com.amos.http;

import com.amos.json.JsonObject;

import java.io.*;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
//import java.net.SocketO;

public class HttpResponse implements Response{

	private Socket socket;
	private PrintWriter pw;

	private static final String HEADER="HTTP/1.1 %s\r\n";
	private String status = SC_OK;
	
	StringBuilder header =  new StringBuilder();
	SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
	Map<String,Object> headerMap= new LinkedHashMap<String, Object>();
	private ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	public HttpResponse(Socket socket) throws IOException {
		this.socket=socket;

		//bufferedOutputStream= new BufferedInputStream();
		pw= new PrintWriter(buffer,true);

	}
	


	@Override
	public String getCharacterEncoding() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContentType() {
		return null;
	}

	@Override
	public PrintWriter getWriter() throws IOException {
		return pw;
	}

	@Override
	public void setCharacterEncoding(String charset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContentType(String type) {
		header.append(String.format("Content-Type: %s", type));
		headerMap.put("Content-Type: ", type);
		
	}

	@Override
	public void setBufferSize(int size) {
		buffer= new ByteArrayOutputStream(size);
		
	}

	@Override
	public int getBufferSize() {
		return buffer.size();
	}

	@Override
	public void flushBuffer() throws IOException {
		buffer.flush();
	}

	@Override
	public void resetBuffer() {
		buffer.reset();
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCookie(Cookie cookie) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsHeader(String name) {
		return headerMap.containsValue(name);
	}

	@Override
	public String encodeURL(String url) {
		url =new String(url.getBytes(), StandardCharsets.UTF_8);
		return URLDecoder.decode(url);
	}

	@Override
	public void sendRedirect(String location) throws IOException {	

		pw= new PrintWriter(socket.getOutputStream());
		setStatus(SC_MOVED_TEMPORARILY);
		addHeader("Location", location);
		String head = buildHeder();
		pw.println(head);
		socket.getOutputStream().write(buffer.toByteArray());
		socket.getOutputStream().flush();
	}


	public void forward(String location) throws IOException{
		String head = buildHeder();
		socket.getOutputStream().write(head.getBytes());
		socket.getOutputStream().write(buffer.toByteArray());
		socket.getOutputStream().flush();
	}


	@Override
	public void setDateHeader(String name, long date) {
		headerMap.put(name, date);
		
	}

	@Override
	public void addDateHeader(String name, long date) {
		headerMap.put(name, sdf.format(new Date(date)));
	}

	@Override
	public void setHeader(String name, String value) {
		headerMap.put(name, value);
	}

	@Override
	public void addHeader(String name, String value) {
		headerMap.put(name, value);
		header.append(String.format("%s: %s\r\n",name, value));
	}

	@Override
	public void setIntHeader(String name, int value) {
		headerMap.put(name, value);
		header.append(String.format("%s: %d\r\n",name, value));
		
	}

	@Override
	public void addIntHeader(String name, int value) {
		headerMap.put(name, value);
		header.append(String.format("%s: %d\r\n",name, value));
	}

	@Override
	public String getHeader(String name) {
		return String.valueOf(headerMap.get(name));
	}



	@Override
	public void setStatus(String st) {
		status=st;
	}

	@Override
	public void sendJson(JsonObject jsonObject) throws IOException {
		sendJson(jsonObject.toString());
	}

	@Override
	public void sendJson(String json) throws IOException {
		buffer.reset();
		addHeader("Content-Type","application/json");
		addIntHeader("Content-Length:",json.length());
		String head = buildHeder();
		socket.getOutputStream().write((head.getBytes()));
		socket.getOutputStream().write(json.getBytes());
		socket.getOutputStream().flush();
	}

	@Override
	public void includeJavaScript(File file) throws IOException {
		pw.write("<script type=\"text/javascript\">");
		byte[] js= Files.readAllBytes(Paths.get(file.getPath()));
		pw.write(new String(js));
		pw.write("</script>");
	}

	public String buildHeder(){
		StringBuilder sb = new StringBuilder(String.format(HEADER, status));
		
		Set<Map.Entry<String, Object>> it=headerMap.entrySet();
		
		for(Map.Entry<String, Object> element : it){
			sb.append(String.format("%s: %s\r\n",element.getKey(),String.valueOf(element.getValue())));
		}
		return sb.append("\r\n").toString();
	}

	@Override
	public byte[] getBuffer() {
		return buffer.toByteArray();
	}


	@Override
	public OutputStream getOutputStream() throws IOException {
		return socket.getOutputStream();
	}

}
