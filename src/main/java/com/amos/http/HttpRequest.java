package com.amos.http;

import com.amos.json.JsonObject;
import com.amos.json.JsonParser;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;



public class HttpRequest implements Request{

	private InputStream is;
	private int         sizeInputData;
	private BufferedReader br;
	private String url;

	private Socket socket;
	private String method;
	private String protocol;
	private String host;
	private HashMap<String, String> data= new HashMap<String, String>();

	private JsonObject jsonObject;
	private HashMap<String, String> header= new HashMap<String, String>();

	private Object lock = new Object();
	
	public  HttpRequest(Socket socket) throws IOException {
		synchronized (lock) {
		this.socket=socket;
		is=socket.getInputStream();
		br= new BufferedReader(new InputStreamReader(is,"UTF-8"));

		String line=null;
		try {



		if((line=br.readLine())!=null){
			System.out.println(line);
		    parseFirstLine(line);
		    line=br.readLine();
			
	     do{

		     System.out.println(line);
		     String[] datas=line.split(": ");
             header.put(datas[0],datas[1]);
		     line=br.readLine();

		     if("".equals(line))
		     	break;
	     }while(br.ready());
	     
	     
	     String len;
	     if((len=header.get("Content-Length"))!=null) {
			 int dataLen = Integer.parseInt(len);
			 StringBuilder sb = new StringBuilder();
			 for (int i = 0; i < dataLen; i++)
				 sb.append((char) br.read());


			 parseData(sb.toString());
		 }
	     }
}catch (Exception e){}
			}
      }
	
	private void parseData(String data){

		if("".equals(data))
			return;
		if(data.charAt(0)=='{'){
			jsonObject= new JsonParser().saxParser(data).getAsJsonObject();
			return;
		}

		String[] fres= data.split("&");
		for(String s : fres){
			String[] temp = s.split("=");
			if(temp.length==1)
				this.data.put(temp[0],"");
			else
			    this.data.put(temp[0], temp[1]);
		}
	}
	
	
	
	private void parseFirstLine(String line){
		String[] temp =line.split(" ");
		method=temp[0];
		url=temp[1];
		if(url.contains("?")){
		    String[] params=url.split("\\?");
		    url=params[0];
		    if(params.length!=0){
		        parseData(params[1]);
            }
        }

		protocol=temp[2];
	}
	
	@Override
	public String getCharacterEncoding() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCharacterEncoding(String env) throws UnsupportedEncodingException {
		
	}

	@Override
	public int getContentLength() {
		return sizeInputData;
	}

	@Override
	public String getContentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getParameter(String name) {
		return data.get(name);
	}

	@Override
	public Enumeration<String> getParameterNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getParameterValues(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProtocol() {
		return protocol;
	}

	@Override
	public String getScheme() {
		return null;
	}

	@Override
	public String getServerName() {
		return "AMOS 1.1";
	}

	@Override
	public int getServerPort() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BufferedReader getReader() throws IOException {
		return br;
	}

	@Override
	public String getRemoteAddr() {
		return socket.getRemoteSocketAddress().toString();
	}

	@Override
	public String getRemoteHost() {
		return socket.getRemoteSocketAddress().toString();
	}

	@Override
	public void setAttribute(String name, Object o) {
		// TODO Auto-generated method stub
		
	}

	public boolean isImage(){
		return url.endsWith("jpg");
	}

	@Override
	public void removeAttribute(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getRemotePort() {
		return socket.getPort();
	}

	@Override
	public String getLocalName() {
		return socket.getLocalAddress().getHostName();
	}

	@Override
	public String getLocalAddr() {
		return socket.getRemoteSocketAddress().toString();
	}

	@Override
	public String getRemoteUser() {
		return socket.getRemoteSocketAddress().toString();
	}

	@Override
	public String getQueryString() {
		return host+url;
	}

	@Override
	public String getResourcessName() {
		return url;
	}

	@Override
	public String getMethod() {
		return method;
	}

	@Override
	public String getHeader(String name) {
		return header.get(name);
	}

	@Override
	public Cookie[] getCookies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getLocalPort() {
		return socket.getLocalPort();
	}


	@Override
	public String getUserAgent() {
		return header.get("User-Agent");
	}

	@Override
	public Session getSession() {
		return Session.getInstance();
	}

	@Override
	public JsonObject getJson() {
		return jsonObject;
	}

}
