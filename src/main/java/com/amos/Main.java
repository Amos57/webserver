package com.amos;


import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;


import com.amos.config.Startup;
import com.amos.run.Run;

public  class Main {



	public static void main(String[] args) throws IOException{

		Startup config= Startup.getInstance();
		config.scannerClasses();

		ServerSocket ss = new ServerSocket(config.getPort());

		while(true)
		try(Socket socket=ss.accept()){
			System.out.println(socket.getRemoteSocketAddress());
			new Run(socket).run();
			
		}catch (Exception e) {
			e.printStackTrace();
			
			break;
		}

	}

}
